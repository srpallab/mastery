const randomNumbers = [20, 5, 654, 7, 44, -9, 78, 44, 556];

function stopWhenNagetive(numbers) {
  const positiveNumbers = [];
  for(let number of numbers) {
    if (number < 0) {
      break;
    }
    positiveNumbers.push(number);
  }
  return positiveNumbers;
}

console.log(stopWhenNagetive(randomNumbers));
