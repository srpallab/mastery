function computeLPSArray(pattern, patternLength, lpsArray) {
  len = 0;
  i = 1;
  while (i < patternLength){
    if (pattern[i] == pattern[len]) {
      lpsArray[i] = ++len;
      i++;
    } else {
      if (len != 0) {
	len = lpsArray[len - 1];
      } else {
	lpsArray[i] = 0;
	i++;
      }
    }
  }
}

function KMPSearch(pattern, text) {
  const textLenght = text.length;
  const patternLength = pattern.length;

  let lpsArray = Array(patternLength).fill(0);
  // console.log(lpsArray);
  computeLPSArray(pattern, patternLength, lpsArray);
  // console.log(lpsArray);
  let textIndex = 0;
  let patternIndex = 0;
  while (textIndex < textLenght){
    // console.log(textIndex);
    if (text[textIndex] == pattern[patternIndex]) {
      textIndex++;
      patternIndex++;
    } else {
      if (patternIndex != 0) {
	patternIndex = lpsArray[patternIndex - 1];
      } else {
	textIndex++;
      }
    }
    if (patternIndex == patternLength) {
      console.log(textIndex - patternIndex);
      patternIndex = lpsArray[patternIndex - 1];
    }
  }
}


text = "onionionspl";
pattern = "onions";

KMPSearch(pattern, text);
