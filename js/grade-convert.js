function findGrade(number){
  let grade;
  switch(true){
    case number < 33:
      grade = 'F';
      break;
    case (number >= 33 && number <= 40):
      grade = 'D';
      break;
    case (number >= 40 && number <= 50):
      grade = 'C';
      break;
    case (number >= 50 && number <= 60):
      grade = 'B';
      break;
    case (number >= 60 && number <= 70):
      grade = 'A';
      break;
    case (number >= 80):
      grade = 'A+';
      break;
  }
  return grade;
}

let myGrade = findGrade(80);

console.log(myGrade);
