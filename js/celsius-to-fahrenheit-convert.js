function celsiusToFahrenheit(celsius){
  let fahrenheit = celsius * 1.8 + 32;
  return fahrenheit;
}

const todaysTempInFahrenheit = celsiusToFahrenheit(41);

console.log(todaysTempInFahrenheit, "F");
