function fahrenheitToCelsius(fahrenheit){
  let celsius = (fahrenheit - 32) / 1.8 ;
  return celsius;
}

const todaysTempInCelsius = fahrenheitToCelsius(100);

console.log(todaysTempInCelsius, "C");
