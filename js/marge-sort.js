function marge(mainArray, leftArray, rightArray) {
  var mainArrayIndex = 0;
  var leftArrayIndex = 0;
  var rightArrayIndex = 0;
  var leftArrayLength = leftArray.length;
  var rightArrayLength = rightArray.length;
  
  while(leftArrayIndex < leftArrayLength && rightArrayIndex < rightArrayLength){
    if (leftArray[leftArrayIndex] < rightArray[rightArrayIndex]) {
      mainArray[mainArrayIndex] = leftArray[leftArrayIndex];
      leftArrayIndex++;
    } else{
      mainArray[mainArrayIndex] = rightArray[rightArrayIndex];
      rightArrayIndex++;
    }
    mainArrayIndex++;
  }
  while(leftArrayIndex < leftArrayLength){
    mainArray[mainArrayIndex] = leftArray[leftArrayIndex];
    leftArrayIndex++;
    mainArrayIndex++;
  }
  while(rightArrayIndex < rightArrayLength){
    mainArray[mainArrayIndex] = rightArray[rightArrayIndex];
    rightArrayIndex++;
    mainArrayIndex++;
  }
}

function margeSort(array){
  arrayLength = array.length;
  // console.log(arrayLength);
  if (arrayLength < 2) {
    return;
  }
  var arrayMiddle = Math.floor(arrayLength/2);
  var leftArray = [];
  var rightArray = [];
  for (var i = 0; i < arrayMiddle; i++) {
    leftArray.push(array[i]);
  }
  // console.log(leftArray);
  for (var j = arrayMiddle; j < arrayLength; j++) {
    rightArray.push(array[j]);
  }
  // console.log(rightArray);
  margeSort(rightArray);
  margeSort(leftArray);
  marge(array, leftArray, rightArray);
}


inputNumbersOne = [1001, 4, 5, 6, 3, 2, 1, 55, 9, -3, -1, 1008, 5005, 25];
inputNumbersTwo = [2, 3, 6, 6, 5];
margeSort(inputNumbersOne, inputNumbersOne.length);
margeSort(inputNumbersTwo, inputNumbersTwo.length);
console.log(inputNumbersOne);
console.log( inputNumbersTwo);
