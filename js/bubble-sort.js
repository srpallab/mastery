function bubbleSort(array, arrayLength) {
  for (var i = 0; i < (arrayLength - 1); i++) {
    var flag = 0;
    for (var j = 0; j < (arrayLength - i - 1); j++) {
      if (array[j] > array[j+1]) {
	[array[j], array[j + 1]] = [array[j + 1], array[j]];
	flag++;
      }
    }
    if (flag == 0) {
      break;
    }
  }
}

inputNumbersOne = [1001, 4, 5, 6, 3, 2, 1, 55, 9, -3, -1, 1008, 5005, 25];
inputNumbersTwo = [2, 3, 6, 6, 5];
bubbleSort(inputNumbersOne, inputNumbersOne.length);
bubbleSort(inputNumbersTwo, inputNumbersTwo.length);
console.log(inputNumbersOne);
console.log( inputNumbersTwo);
