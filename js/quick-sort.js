function partitioning(array, start, end) {
  let pivot = array[end];
  let swapIndex = start;
  for (var i = start; i < end; i++) {
    if (array[i] <= pivot) {
      [array[swapIndex], array[i]] = [array[i], array[swapIndex]];
      swapIndex++;
    }
  }
  [array[end], array[swapIndex]] = [array[swapIndex], array[end]];
  return swapIndex;
}

function quickSort(array, start, end) {
  // console.log(start, end);
  if(start < end){
    let pivotIndex = partitioning(array, start, end - 1);
    // console.log(pivotIndex);
    quickSort(array, start, pivotIndex);
    quickSort(array, pivotIndex+1, end);
  } 
}

inputNumbersOne = [1001, 4, 5, 6, 3, 2, 1, 55, 9, -3, -1, 1008, 5005, 25];
inputNumbersTwo = [2, 3, 6, 6, 5];
quickSort(inputNumbersOne, 0, inputNumbersOne.length);
quickSort(inputNumbersTwo, 0, inputNumbersTwo.length);
console.log(inputNumbersOne);
console.log( inputNumbersTwo);
