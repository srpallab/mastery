function insertionSort(array, arrayLength) {
  for (var i = 0; i < arrayLength; i++) {
    var value = array[i];
    var hole = i;
    while(hole > 0 && array[hole - 1] > value){
      array[hole] = array[hole - 1];
      hole--;
    }
    array[hole] = value;
  }
}


inputNumbersOne = [1001, 4, 5, 6, 3, 2, 1, 55, 9, -3, -1, 1008, 5005, 25];
inputNumbersTwo = [2, 3, 6, 6, 5];
insertionSort(inputNumbersOne, inputNumbersOne.length);
insertionSort(inputNumbersTwo, inputNumbersTwo.length);
console.log(inputNumbersOne);
console.log( inputNumbersTwo);
