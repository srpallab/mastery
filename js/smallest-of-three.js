function findLargestOfThree(numOne, numTwo, numThree) {
  let largest;
  if (numOne < numTwo && numOne < numThree) {
    largest = numOne;
  } else if (numTwo < numThree) {
    largest = numTwo;
  } else {
    largest = numThree;
  }
  return largest;
}

console.log(findLargestOfThree(1, 2, 3));
console.log(findLargestOfThree(3, 2, 3));
console.log(findLargestOfThree(1000, -2, 388));
