function bestFriend(friends) {
  let bestFriendName = friends[0];
  for(let friend of friends) {
    if (friend.length > bestFriendName.length) {
      bestFriendName = friend;
    }
  }
  return bestFriendName;
}

const myFriends = ['Saidul Alam', 'Sabah bin hossain', 'Jabah bin Hossain', 'Mudassir Masum Halim'];

console.log(bestFriend(myFriends));
