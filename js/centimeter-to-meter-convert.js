function centimeterToMeter(centimeter) {
  const meter = centimeter * 0.01;
  return meter.toFixed(2);
}


const tableLenghtInMeter = centimeterToMeter(112);
console.log('My Table Lenght in Meter', tableLenghtInMeter);
