function feetToInch(feet) {
  const inch = feet * 12;
  return inch.toFixed(1);
}

const myHeightInInch = feetToInch(5.9);
console.log('My Height in inch', myHeightInInch);
