function paperRequirements(firstBookNumbers, secondBookNumbers, thirdBookNumbers) {
  const firstBookPageRequirments = 100;
  const secondBookPageRequirments = 200;
  const thirdBookPageRequirments = 300;

  const totalBookPageRequirments = (
    firstBookNumbers * firstBookPageRequirments
  ) + (
    secondBookNumbers * secondBookPageRequirments
  ) + (
    thirdBookNumbers * thirdBookPageRequirments
  );
  
  return totalBookPageRequirments;
}


console.log(paperRequirements(30, 10, 10));
