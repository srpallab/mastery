#+title: SQL and MySQL Basics.
#+author: Shafiqur Rahman
* SQL (Structured Query Language)
   - SQL use for CURD operations
   - CURD: Create, Update, Read, Delete
   - *Database Concept 1*: SQL is a language standard, but there is many
     implementation in the form of different relational database
     management system.
   - *Database Concept 2*: Relational database store their data in tables.
   - *Database Concept 3*: Every table has a set of column. Each
     column has a name and a datatype, at least.
   - *Database Concept 4*: Every Entry in a table is called
     *record*. A table can have multiple records. The intersection of
     a column and a record is a *field*.
   - *Database Concept 5*: *NULL* is the *SQL* value for no value. A
     field of any type can be *NULL*.
   - *Database Concept 6*: Every Table should have a *primary key*, a
     column that uniquely identifies each row. It can never be null,
     and must be set on record creation and never changed. 
   - *Database Concept 7*: A foreign key is a column that links one
     table to another by only accepting values that are already in a
     specified column of second table.
* DBMS (Database Management System)
   - Program that interact with the database.
   - Create and Manage Database
   - Store, Modify and extract Data.
   - Interface Between the database and database application or user
     [[./Database.png]]
   - DBMS Manages three import things
     - The Data
     - Data Engine (Storage Engine)
       - This engine store and retrieves data.
       - Most DBMS includes Application Programming Interface(API) to
         directly control the database engines.
       - Example: ~InnoDB~, ~MyISAM~ 
     - Database Schema
   - Types of DBMS
     - Relational Databases (RDBMS)
     - NoSQL DBMS
     - In Memory Database Management System (IMDBMS)
     - Columnar Database Management System (CDBMS)
     - Cloud Based Database Management System
* Data Definition Language (~DDL~)  
   - ~DDL~ change the database structure.
   - Manage database objects like tables and columns.
   - Tables and Columns, are created, modified or removed by DDL.
   - Example: CREATE, ALTER, DROP etc
** Database
  - Database is collection of one or more tables.
  - Database name must contain at least one alphabet letter.
  - Database name always stores in small letters.
  - Show databases
    #+BEGIN_SRC sql
      SHOW DATABASES;
    #+END_SRC
  - Create a databases
    #+BEGIN_SRC sql
      CREATE DATABASE database_name;
    #+END_SRC
  - Use a database
    #+BEGIN_SRC sql
      USE database_name;
    #+END_SRC
  - Delete a database
    #+BEGIN_SRC sql
      DROP DATABASE database_name;
    #+END_SRC
** Database Table
  - Relational Databases consists of Tables
  - Each Row of Table is called *Record*
  - Each Column of Table is called *Fields*
  - Collection of Tables in Databases is called *Schema*
  - The Database table names are case in-sensitive. That means
    'student' is same as "STUDENT"
  - Table name must be unique.
  - Show tables
    #+BEGIN_SRC sql
      SHOW TABLES;
    #+END_SRC
  - Create table
    #+BEGIN_SRC sql
      CREATE TABLE users(
	     user_id int NOT NULL PRIMARY KEY,
	     first_name VARCHAR(100) NOT NULL,
	     last_name VARCHAR(100) NOT NULL,
	     email VARCHAR(255) NOT NULL
      );
    #+END_SRC
  - Show details of a table
    #+BEGIN_SRC sql
      DESCRIBE users;
      EXPLAIN users;  
    #+END_SRC
  - Rename table
    #+BEGIN_SRC sql
      ALTER TABLE users RENAME user;
    #+END_SRC
  - Add column in a table
    #+BEGIN_SRC sql
      ALTER TABLE users ADD encrypted_password VARCHAR(255);
    #+END_SRC
  - Rename column in a table
    #+BEGIN_SRC sql
      ALTER TABLE users CHANGE encryped_password password VARCHAR(255) 
    #+END_SRC
  - Delete column in a table
    #+BEGIN_SRC sql
      ALTER TABLE users DROP COLUMN email;
    #+END_SRC
  - Delete Table
    #+BEGIN_SRC sql
      DROP TABLE users;
    #+END_SRC
** Constraints
   - SQL Constraints defines specific rules to the column data in a
     database table.
   - If Constraint rules are not followed when inserting, deleting or
     updating database gives a error.
   - SQL Constraints can be addend while creating table or altering a
     table.
   - Standard SQL Supports SIX SQL Constraints.
*** NOT NULL
    - By default database column can store NULL Value.
    - NOT NULL prevents that to happening.
    - Add NOT NULL:
      #+begin_example sql
	CREATE TABLE user (id INT NOT NULL);
      #+end_example
    - Add NOT NULL later:
      #+begin_example sql
	ALTER TABLE user MODIFY password VARCHAR(255) NOT NULL;
      #+end_example
*** UNIQUE
    - UNIQUE Constraint restrict store duplicate value.
    - PRIMARY KEY by default set UNIQUE Constraint for column.
    - Add UNIQUE:
      #+begin_example sql
	CREATE TABLE user(id INT UNIQUE);
      #+end_example
    - Add UNIQUE later:
      #+begin_example sql
	ALTER TABLE tablename ADD UNIQUE(column);
      #+end_example
    - drop UNIQUE:
      #+begin_example sql
        ALTER TABLE tablename DROP INDEX column;
      #+end_example
*** CHECK 
    - It used to control the value range that can be stored in a table
      column.
    - A table with CHECK Constraint defined column will save only
      specific values in the column.
    - CHECK Constraint can be defined single or multiple column.
    - Add CHECK Constraint:
      #+begin_example sql
	CREATE TABLE tablename(
	       col1 datatype,
	       col2 datatype,
	       CHECK (col1 > 0)
	);
      #+end_example
    - Add CHECK Constraint later:
      #+begin_example sql
	ALTER TABLE table ADD CONSTRAINT chk_col CHECK (col > 0);
      #+end_example
*** DEFAULT
    - The DEFAULT is used to set a default value for a data column.
    - If the value is not defined in insert, the default value added.
    - Add DEFAULT:
      #+begin_example sql
	CREATE TABLE table (
	       col1 datatype,
	       col2 datatype DEFAULT 'no value'
	);
      #+end_example
    - Add DEFAULT later:
      #+begin_example sql
	ALTER TABLE table ALTER col1 SET DEFAULT 'no value';
      #+end_example
*** ~AUTO_INCREMENT~ or SEQUENCE
    - ~AUTO_INCREMENT~ attribute generate and saves unique number each
      row insert into table.
    - It works with numerical data.
    - Example:
      #+begin_example sql
        CREATE TABLE user (id INT NOT NULL AUTO_INCREMENT);
      #+end_example
*** PRIMARY KEY
    - Add PRIMARY key
      #+begin_example sql
	CREATE TABLE user (
	       id INT NOT NULL AUTO_INCREMENT,
	       CONSTRAINT user_id PRIMARY KEY (id)
	);
      #+end_example
    - Add PRIMARY KEY later
      #+begin_example sql
	-- Make sure values are unique and not null.
	ALTER TABLE users ADD PRIMARY KEY (id);
	ALTER TABLE users CHANGE id id INT NOT NULL AUTO_INCREMENT;
      #+end_example
    - Drop PRIMARY KEY
      #+begin_example sql
	ALTER TABLE table_name DROP PRIMARY KEY column;
      #+end_example
*** FOREIGN KEY
    - Add FOREIGN KEY
      #+begin_example sql
	CREATE TABLE order(
	       id INT NOL NULL AUTO_INCREMENT,
	       customer_id INT NOT NULL,
	       CONSTRAINT customer_fk FOREIGN KEY (customer_id)
	       REFERENCES customer(id) ON DELETE CASCADE);
      #+end_example
    - Add FOREIGN KEY
      #+begin_example sql
	ALTER TABLE table1 ADD CONSTRAINT fk_col FOREIGN KEY (col)
	      REFERENCES table2(col); 
      #+end_example
    - Drop FOREIGN KEY
      #+begin_example sql
	ALTER TABLE table DROP FOREIGN KEY fk_col;
      #+end_example
* Data Control Language(~DCL~)
  - *Privileges*: Add User, Create Database, Create Table, Perform Queries
  - The *GRANT* Statement gives user permissions and *REVOKE*
    Statement withdraw permissions
** Create User 
   - Create User With MySQL
     #+BEGIN_SRC sql
       CREATE USER 'pal' IDENTIFIED BY 'P@ssw0rd';
     #+END_SRC
   - Change Password
     #+BEGIN_SRC sql
       ALTER USER 'pal' IDENTIFIED BY 'P@ssw0rd123';
     #+END_SRC
   - Show privileges 
     #+begin_example sql
       SELECT User, Select_priv, Insert_priv, Update_priv, Delete_priv,
	      Create_priv FROM user;
     #+end_example
   - Giving Permissions 
     - GRANT ALL = all privileges (create, delete, read, update)
     - ON ~database.table~ = * means all the database or table
     - TO user = name of the user
     - GRANT OPTION = can create user
     - Example
       #+BEGIN_SRC sql
	 GRANT ALL ON *.* TO 'pal' WITH GRANT OPTION;
	 GRANT SELECT, CREATE ON database.* TO 'pal';
       #+END_SRC
   - Taking Back Permissions
     - Example:
       #+begin_example sql
	 REVOKE ALL ON *.* FROM 'pal' WITH GRANT OPTION;
	 REVOKE SELECT, CREATE ON database.* FROM 'pal';
       #+end_example
   - Refresh Permissions
     #+BEGIN_SRC sql
       FLUSH PRIVILEGES;
     #+END_SRC
* Data Manipulation Language (~DML~)
  - DML only changes the data.
  - Data inside a table inserted, update and deleted
  - Example: CREATE, UPDATE and DELETE.
** Data Types
*** Number
     - *Integer*
      #+caption: Integer Values
    | Type      | Storage |     Minimum (Signed) |    Maximum (Signed) | Minimum (Unsigned) |   Maximum (Unsigned) |
    |-----------+---------+----------------------+---------------------+--------------------+----------------------|
    | TINYINT   |       1 |                 -128 |                 127 |                  0 |                  255 |
    | SMALLINT  |       2 |               -32768 |               32767 |                  0 |                65535 |
    | MEDIUMINT |       3 |             -8388608 |             8388607 |                  0 |             16777215 |
    | INT       |       4 |          -2147483648 |          2147483647 |                  0 |           4294962795 |
    | BIGINT    |       8 | -9223372036854775808 | 9223372036854775807 |                  0 | 18446744073709551615 |

    - *Fixed Point Number*
      - DECIMAL(precision, scope)
      - NUMERICAL(precision, scope)
    - *Floating Point Number* (Approximate Value)
      - FLOAT(Mantissa, Exponent)
      - REAL(Mantissa, Exponent)
      - DOUBLE PRECISION(Mantissa, Exponent)
*** String
    - CHAR(length)
    - ~VARCHAR(lenght)~
    - ~NCHAR(length)~ (National Character)
      - It used for ~utf-8~ and Unicode characters. 
    - ~NVARCHAR(lenght)~ (National Variable Character)
      - It used for ~utf-8~ and Unicode characters.
    - CLOB (Character Large Object)
      - It use to store large data like post in plain text, HTML or
        XML format.
      - It used to store character data.
      - In MySQL TEXT data type is similar to CLOB.
    - BLOB (Binary Large Object)
      - It use to store large data like post in plain text, HTML or
        XML format.
      - It is used to store binary data like Image, Audio, Video 
*** Date and Times (Temporal)
    - date
    - time
    - ~timestamp~
*** Boolean 
    - ~bool~
    - ~boolean~
** Keywords, Identifiers, Constants
*** Keywords
    - SQL Standard words used to construct the SQL statements.
    - Some Keywords are optional while some are mandatory.
*** Identifiers
    - Names we give to the databases, tables or columns
*** Constants
    - Literals representing fixed values 
*** Example
    #+BEGIN_SRC sql
      -- Keywords: SELECT, FROM, WHERE, =
      -- Identifiers: name, students
      -- Constant: 5
      -- Clauses: SELECT, FROM, WHERE
      SELECT name FROM students WHERE id=5;
    #+END_SRC
** Clauses
   Portion of a SQL statement. Clause name corresponds to the SQL Keyword.
*** SELECT Clause
     - SELECT Clause is the first clause in any SELECT Statement
     - Main purpose is to retrieve data from the database table and
       return it in a tabular structure. 
     - It defines the columns that will be returns final result
     - It Executes after the FORM Clause and any optional WHERE,
       GROUP BY and HAVING Clauses.
     - The FORM Clause builds an intermediate tabular result set from
       which the SELECT Clause ultimately select the data to be
       return. GROUP BY Clause changes the structure of this
       intermediate table.
     - General SELECT Statement
       #+begin_example sql
	 SELECT expression(s) involving keywords, identifiers, and costants
	 FROM tabular structure(s)
	 [WHERE clause]
	 [GROUP BY clause]
	 [HAVING cluse]
	 [ORDER BY cluse]
       #+end_example
     - Display or Read data
	#+BEGIN_SRC sql
	  SELECT * FROM movies;
	  -- or
	  SELECT title FROM movies;
	#+END_SRC
     - SELECT DISTINCT SQL statement used to return only distinct or
       different values from a table column
       #+BEGIN_SRC sql
	 SELECT DISTINCT first_name, age FROM student;
       #+END_SRC
*** FROM Clause
     - FROM Clause can be simple or can be complex
     - The FORM Clause produce a tabular structure also called as
       the *Result Set* or an *Intermediate Result Set*
     - FORM is the first clause that the database system looks at
       when it parses the SQL Statement.
     - FORM Clause can return the result set of one table or more
       than one table using JOIN, VIEW and SUB QUERIES.   
*** WHERE Clause
     - Acts as a filter on the rows of the *Result Set* produce by
           the FORM Clause.
     - WHERE Clause mainly depends upon a condition which evaluates
       as either be TRUE, FALSE or UNKNOWN.  
     - Example
       #+BEGIN_SRC sql
	 SELECT * FROM users WHERE join_date > '2005-01-01';
	 SELECT * FROM users WHERE join_date > '2005-01-01' 
			       AND join_date < '2010-01-01';
       #+END_SRC
*** GROUP BY Clauses
     - GROUP BY Clause groups the data together.
     - The FORM and WHERE Clause creates intermediate tabular *result
       set* and GROUP BY Clause systematically groups the data.
     - The GROUP BY Clause can group the *result set* by one or more
       column.
     - Example:
       #+begin_example sql
	 SELECT users_id, SUM(grade) FROM users GROUP BY users_id;
       #+end_example
*** HAVING Clause
     - The HAVING Clause was added to SQL as WHERE could not be used
       with aggregated data rows.
     - The HAVING Clause filter the group rows produce by the GROUP BY Clause.
     - WHERE Clause filters the intermediate data result rows, while
       HAVING Clause operates on group rows.
     - The HAVING Clause uses conditions and operators to build
       complex SQL Statement.
     - Since the HAVING Clause acts as a filter on group rows, the
       only possible column in group
     - Example
       #+begin_example sql
	 SELECT course, COUNT(*) FROM student GROUP BY course
					      HAVING COUNT(*) < 20;  
       #+end_example
*** ORDER BY Clause
     #+begin_example sql
       -- ascending order
       SELECT title, price, FROM movies ORDER BY price ASC;
       -- descending order
       SELECT title, price, FROM movies ORDER BY price DESC;
     #+end_example
*** LIMIT or TOP Clause and OFFSET Clause
     #+begin_example sql
       -- MySQL
       SELECT * FROM users LIMIT 5;
       SELECT * FROM users LIMIT 5 OFFSET 5;
       -- MS Access or SQL Server
       SELECT TOP 5 FROM student;
       -- Oracle
       SELECT * FROM users WHERE ROWNUM <= 5;
     #+end_example
*** Distinct Clause
     #+begin_example sql
       SELECT DISTINCT join_date FROM users;
     #+end_example
*** ALIASES 
     #+begin_example sql
       SELECT c.title, c.full_name FROM cases AS c; 
     #+end_example
** Operators 
*** Comparison
    - Equals (=)
    - Greater Than (>)
    - Less Than (<)
    - Greater Than, Equals To (>=)
    - Less Than, Equals To (<=)
    - Not Equals To (<>) (!=)
*** Logical
    - AND (&&)
      #+begin_example sql
	SELECT * FROM student WHERE (class = 'first' AND age = 5);
      #+end_example
    - OR (||)
      #+begin_example sql
	SELECT * FROM student WHERE (age = 5 OR age = 7);
      #+end_example
    - NOT
      #+begin_example sql
	SELECT * FROM student WHERE NOT (age = 5 OR age = 7);
      #+end_example
    - IN
      #+begin_example sql
	SELECT * FROM student WHERE age IN(5, 7);
      #+end_example
    - NOT IN
      #+begin_example sql
	SELECT * FROM employee WHERE salary is NOT IN (3200, 3400, 3500);
      #+end_example
    - EXISTS
      #+begin_example sql
	-- Work on Every employee id like loop.
	SELECT * FROM employee WHERE EXISTS
	(SELECT * FROM project WHERE project.employeeid = employee.employeeid);
      #+end_example
    - NOT EXISTS
      #+begin_example sql
	SELECT * FROM employee WHERE NOT EXISTS
	(SELECT * FROM project WHERE project.employeeid = employee.employeeid);
	-- another example
	SELECT ename FROM employee WHERE NOT EXISTS
	  ((SELECT pid FROM project) EXCEPT
	      (SELECT pid FROM work_project WHERE
		 wrok_project.eid = employee.eid));
      #+end_example
    - LIKE: there are two wildcards-- zero or more (%) and only one (_)
      #+BEGIN_SRC sql
	-- ALL Names starts with C
	SELECT * FROM users WHERE first_name LIKE "C%" ;
	-- ALL Numbers starts with 017
	SELECT * FROM users WHERE phone_number LIKE "017%" ;
	-- All Names having u in it
	SELECT * FROM users WHERE first_name LIKE "%u%" ;
	-- All the Names Ends with C
	SELECT * FROM users WHERE first_name LIKE "%C" ;
      #+END_SRC
    - BETWEEN
      #+begin_example sql
	SELECT * FROM student WHERE age BETWEEN 6 AND 10;
      #+end_example
    - UNION
      #+begin_example sql
	-- Returns colums with DISTINCT values
	-- Select Statment must be same column and same data type
	SELECT * FROM Table1 UNION SELECT * FROM Table2;
      #+end_example
    - UNION ALL
      #+begin_example sql
	-- Returns colums with all values
	-- Select Statment must be same column and same data type
	SELECT * FROM Table1 UNION ALL SELECT * FROM Table2;
      #+end_example
    - ALL
    - ANY
    - SOME
*** Numeric
    - Addition (+)
      #+begin_example sql
	-- Add two number and show
	SELECT 10 + 25;
	-- Display Bonaus Column
	SELECT *, (salary * 100)/50 AS Bonuas FROM employee; 
      #+end_example
    - Subtraction (-)
      #+begin_example sql
	-- Add two number and show
	SELECT 25 - 10;
	--
      #+end_example
    - Multiplication (*)
      #+begin_example sql
	-- Add two number and show
	SELECT 10 * 25;
	--
      #+end_example
    - Division (/)
      #+begin_example sql
	-- Add two number and show
	SELECT 25/5;
	--
      #+end_example
*** Concatenation 
    - The Concatenation operator only used for string
    - It joins two strings together.
    - In MySQL two string add with ~CONCAT~ function
    - Example
      #+begin_example sql
	SELECT first_name ||','|| last_name AS full_name FROM student;
	SELECT CONCAT(first_name, ' ', last_name) AS full_name FROM student;
      #+end_example
*** Temporal 
    - Intervals are used while using date and time in SQL.
    - Example
      #+begin_example sql
	-- CURRENT_DATE represents current time on server
	SELECT CURRENT_DATE + INTERVAL 7 DAY AS week;
	SELECT CURRENT_DATE + INTERVAL 1 DAY AS tommorow;
      #+end_example
** Operations on data
*** INSERT INTO Statement
    #+begin_example sql
      INSERT INTO movies (1, 'Gattaca', 'Movie or documentary', 4.99);
      -- OR
      INSERT INTO movies (movies_id, title, description, price)
	   VALUES (1, 'Gattaca', 'Movie or documentary', 4.99);
      -- OR
      INSERT INTO movies (movies_id, title, description, price) VALUES
		  (1, 'Gattaca', 'Movie or documentary', 4.00),
		  (2, 'Now You See Me', 'Magic Related Movie', 4.50),
		  (3, 'Astro Boy', 'Animation Movie', 5.00);
    #+end_example
*** INSERT INTO SELECT Statement
    - Copy data from one table to another table
    - Copy from source table and paste to target table both actions
      done in one single SQL Statement
    - The source table and target table must have similar definition.
    - Any existing data rows in target table remain unaffected.
    - Example:
      #+begin_example sql
	-- Insert Data with complex query
	INSERT INTO users SELECT 10, first_name, last_name,
		  'new@example.com' FROM users WHERE user_id=3;
      #+end_example
*** UPDATE Statement
    - Example:
      #+begin_example sql
	UPDATE movies SET price = 0.9 WHERE title = 'Gattaca';
      #+end_example
*** DELETE Statement
    - Example:
      #+begin_example sql
	DELETE FROM movies WHERE title = 'Gattaca';
      #+end_example
** SUB QUERY
   #+begin_example sql
     SELECT * FROM grade WHERE user_id = (
	    SELECT id FROM student WHERE name="Pal");
     -- Adding two table (They have to be same columns)
     SELECT first_name, last_name FROM student UNION SELECT
		   first_name, last_name FROM teachers;

   #+end_example
** JOIN
*** CROSS JOIN
    - It returns all rows using direct product.
    - Example:
      #+caption: CROSS JOIN
    | LEFT Table |   | RIGHT Table |   | LEFT Table | RIGHT Table |
    |------------+---+-------------+---+------------+-------------|
    | x1         |   | y1          |   | x1         | y1          |
    | x2         | + | y2          | = | x1         | y2          |
    |            |   |             |   | x2         | y1          |
    |            |   |             |   | x2         | y2          |
    #+begin_example sql
      SELECT * FROM Table1, Table2;
    #+end_example 
*** INNER JOIN (~EQUI~ JOIN)
    - It returns all the data rows when there is at least one match
      in both tables.
    - The Column value in a row of Table1 is equals to Column value
      in a row of Table2.
    - In INNER JOIN the ON Clause defines the Columns and condition
      to be evaluate.
    - Example:
      #+caption: INNER JOIN
    | Table1 |   | Table2 |   | Table1 | Table2 |
    |--------+---+--------+---+--------+--------|
    |     11 |   |     15 |   |     12 |     12 |
    |     12 | + |     12 | = |     15 |     15 |
    |     13 |   |     20 |   |        |        |
    |     14 |   |        |   |        |        |
    |     15 |   |        |   |        |        |
    #+begin_example sql
      SELECT * FROM TABLE1 t INNER JOIN table2 b ON t.col = b.col;
    #+end_example
*** OUTER JOIN
    - It can return matched as well as unmatched data rows.
    - Three types of OUTER JOIN
**** LEFT OUTER JOIN
     - It returns all data rows from left table
     - It also returns unmatched rows from left table and put right
       table rows value NULL.
     - Example:
       #+caption: LEFT OUTER JOIN
     | Table1 |   | Table2 |   | Table1 | Table2 |
     |--------+---+--------+---+--------+--------|
     |     11 |   |     12 |   |     12 | 12     |
     |     12 |   |     15 |   |     15 | 15     |
     |     13 | + |     50 | = |     11 | NULL   |
     |     14 |   |        |   |     13 | NULL   |
     |     15 |   |        |   |     14 | NULL   |
     #+begin_example sql
       SELECT * FROM table1 LEFT OUTER JOIN table2 ON table1.col = table2.col;
     #+end_example

**** RIGHT OUTER JOIN
     - It returns all data rows from right table.
     - It also returns unmatched rows from right table and put left
       table rows value NULL. 
     - Example:
       #+caption: LEFT OUTER JOIN
     | Table1 |   | Table2 |   | Table1 | Table2 |
     |--------+---+--------+---+--------+--------|
     |     11 |   |     12 |   |     12 |     12 |
     |     12 |   |     15 |   |     15 |     15 |
     |     13 | + |     50 | = |   NULL |     50 |
     |     14 |   |        |   |        |        |
     |     15 |   |        |   |        |        |
     #+begin_example sql
       SELECT * FROM table1 RIGHT OUTER JOIN table2 ON table1.col = table2.col;
     #+end_example

**** FULL OUTER JOIN
     - It returns all data rows from both left and right table
     - It also returns unmatched rows from both tables and puts rows
       value NULL.
     - MySQL do not have any FULL OUTER JOIN. We can use UNION for that.
     - Example:
       #+caption: LEFT OUTER JOIN
     | Table1 |   | Table2 |   | Table1 | Table2 |
     |--------+---+--------+---+--------+--------|
     |     11 |   |     12 |   |     12 | 12     |
     |     12 |   |     15 |   |     15 | 15     |
     |     13 | + |     50 | = |     11 | NULL   |
     |     14 |   |        |   |     13 | NULL   |
     |     15 |   |        |   |     14 | NULL   |
     |        |   |        |   |   NULL | 50     |
     #+begin_example sql
       SELECT * FROM table1 FULL OUTER JOIN table2 ON table1.col = table2.col;
     #+end_example
** VIEW
   - VIEW is a database object that can be created like a table.
   - A VIEW is like a virtual table. VIEW do not actually store data.
   - VIEW a complex SELECT Statements used as virtual table for ease
     of references and reuse.
   - VIEW are useful for sorting complex SQL Statement as a virtual
     table and request as a table instead of complex query.
   - We can restrict users from accessing underlying table and
     instead give access to VIEWS with limited column.
   - Since Every time user request VIEW, database engine recreate
     result set which always returns up-to-date data.
   - Example:
     #+begin_example sql
       CREATE VIEW viewname AS SELECT * FROM table WHERE condition;
     #+end_example
   - Delete VIEW
     #+begin_example sql
       DROP VIEW viewname;
     #+end_example
   - Derived Tables or Inline VIEW
     - A Derived Table is a sub query in parentheses from which we can
       select the data column in the main query.
     - The sub query in the parentheses forms a tabular structure
       which is a source of data for outer or main query
     - Every derived table must have a name using AS Keyword.
     - Unlike Views in derived tables the intermediate tables are
       created directly inline or within the SQL.
     - Example:
       #+begin_example sql
	 SELECT col FROM (SELECT * FROM table1 INNER JOIN table2
		ON table1.column = table2.column) AS derivedtable;
       #+end_example
** Indexes
** SQL Functions
   - Some Functions work on only number values, Some Functions also
     works on String also.
   - There are mainly two types of Functions in SQL. *Aggregated
     Functions* and *Scalar Functions*.
   - Aggregated Functions work on group of rows and return single
     result value. Ex: COUNT(), MIN(), MAX(), SUM(), AVG()
   - Scalar Functions work on single value and return single result
     value. Ex: LEN(), ROUND(), SUBSTRING(), CASE(), NOW()
*** COUNT Function
    - The COUNT Function is used to count the data rows returned in
      the result set.
    - It counts *distinct or all values* in the result set.
    - It does not count NULL values.
    - Example:
      #+begin_example sql
	SELECT COUNT(*) AS total_rows FROM table;
	SELECT COUNT(DISTINCT col) AS total FROM table;
      #+end_example
*** SUM Function
    - It will only work on number values and for the string values it
      returns 0 always.
    - Example:
      #+begin_example sql
	SELECT SUM(col) FROM table;
      #+end_example
*** MIN Function
    - It works on both number value and string value.
    - Example:
      #+begin_example sql
	SELECT MIN(col) FROM table;
      #+end_example
*** MAX Function
    - It works on number as well as string values.
    - Example:
      #+begin_example sql
	SELECT MAX(col) FROM table;
      #+end_example
*** AVG Function
    - It will only work on number values and for the string values it
      returns 0 always.
    - Example:
      #+begin_example sql
	SELECT AVG(col) FROM table;
      #+end_example
*** ROUND Function
    - ROUND Function is used to round a floating point number to given
      decimal number. 
    - It will only work on numeric value.
    - Example:
      #+begin_example sql
	SELECT ROUND(col, decimals) FROM table;
      #+end_example
*** FORMAT Function
    - FORMAT Function is used to round a floating point number to the
      given decimal number.  
    - It will only work on numeric values.
    - Example:
      #+begin_example sql
	SELECT FORMAT(col, decimals) FROM table;
      #+end_example
*** UCASE Function
    - UCASE makes string to uppercase. 
    - It will only work on string.
    - Example:
      #+begin_example sql
	SELECT UCASE(col) FROM table;
      #+end_example
*** LCASE Function
    - LCASE makes string to uppercase. 
    - It will only work on string.
    - Example:
      #+begin_example sql
	SELECT LCASE(col) FROM table;
      #+end_example
*** MID Function
    - MID Function extracts characters from the given string data. 
    - It will only work on string.
    - Example:
      #+begin_example sql
	SELECT MID(col, start, length) FROM table;
      #+end_example
*** LENGTH Function
    - LENGTH returns the length of the given string value. 
    - It will only work on string.
    - Example:
      #+begin_example sql
	SELECT LENGTH(col) FROM table;
      #+end_example
*** SUBSTRING Function
    - SUBSTRING returns the subsection of the given string value. 
    - It will only work on string.
    - Example:
      #+begin_example sql
	SELECT SUBSTRING(col FROM start FOR lenght) FROM table;
      #+end_example
*** COALESCE Function
    - The COALESCE Function returns the first non-NULL value from the
      given list. 
    - If there is no NULL value it returns NULL.
    - Example:
      #+begin_example sql
	SELECT COALESCE(NULL, 2, 3);
	SELECT COALESCE(col1, col2, col3) FROM table;
      #+end_example
*** ~CHAR_LENGTH~ Function
    - ~CHAR_LENGTH~ returns the lenght of the given string value. 
    - It will work on both string and numeric.
    - Example:
      #+begin_example sql
	SELECT CHAR_LENGTH(col) FROM table;
      #+end_example
*** CAST Function
    - CAST Function use to change data type of given data.
    - Example:
      #+begin_example sql
	SELECT CAST(col AS datatype) FROM table;
      #+end_example
*** CASE Function
    - The CASE Function returns a value or NULL by evaluating series
      of condition.
    - The CASE Function conditions are made up of keywords WHEN, THEN,
      ELSE and END.
    - It works similar as if else in programming language.
    - It works on both string and numeric values.
    - In Simple Case an expression is compared to static value.
    - In Searched Case an expression is compared to multiple logical
      conditions. 
    - Example:
      #+begin_example sql
	SELECT col1, col2 CASE
	WHEN (col3 condition) THEN result1
	WHEN (col3 condition) THEN result2
	WHEN (col3 condition) THEN result3
	ELSE no_result
	END;
      #+end_example
*** NULLIF Function
    - NULLIF returns NULL Value, if the two parameter values are equal.
    - If the two parameter values are not equal, the NULLIF function
      return the first argument.
    - It works on both string and numeric values.
    - Example:
      #+begin_example sql
	SELECT NULLIF(expression1, expression2) FROM Table;
      #+end_example
** SQL Dates
   - SQL can store date and time in the database
   - NOW(), CURDATE() and CURTIME() which returns date and time,
     current date and current time respectively.
   - Example
     #+begin_example sql
       SELECT NOW();
       SELECT * FROM orders WHERE orderdate = '2020-01-12 11:48:15';
     #+end_example
** SQL Injections
   - SQL Injection is a method where a malicious person can inject
     some SQL commands to display other information or destroy
     database, using web form, or application.
   - SQL Statements are used to manage the database from a web page or
     application. User Interact with database using web form. SQL
     Statements are text keywords and can be changed dynamically.
   - Mainly Three Types of SQL Statements:
     - Using 1=1
       - Form Field:
	 #+begin_example
	   Userid: 15 or 1=1
	 #+end_example
       - PHP Code:
	 #+begin_example php
	   $id = $_POST["id"];
	   $query = "SELECT * FROM user WHERE userid=" . $id;
	 #+end_example
       - Final SQL Statement:
	 #+begin_example sql
	   SELECT * FROM user WHERE userid = 15 or 1=1;
	 #+end_example
     - Using ""=""
       - Form Field:
	 #+begin_example
	   UserName: "or""="
	   Password: "or""="
	 #+end_example
       - PHP Code:
	 #+begin_example php
	   $ur = $_POST["user"];
	   $pw = $_POST["pass"];
	   $query = "SELECT * FROM user WHERE user='".$ur."' AND pw='".$pw."'";
	 #+end_example
       - Final SQL Statement:
	 #+begin_example sql
	   SELECT * FROM user WHERE user=""or"=" AND pw=""or"=";
	 #+end_example
     - Using batched SQL Statements.
       - Form Field:
	 #+begin_example
	   Userid: 15;DROP TABLE students;
	 #+end_example
       - PHP Code:
	 #+begin_example php
	   $id = $_POST["id"];
	   $query = "SELECT * FROM user WHERE userid=" . $id;
	 #+end_example
       - Final SQL Statement:
	 #+begin_example sql
	   SELECT * FROM user WHERE userid = 15;DROP TABLE students;
	 #+end_example
   - Use ~mysqli_real_escape_string~ function to escape the harmful
     characters.
   - Use PHP Data Objects (~PDO~) to safely execute the SQL
     statements.
     #+begin_example php
       $st = $pdo -> prepare("SELECT * FROM user WHERE userid= :id");
       $st -> execute(array('id' => $name)); 
     #+end_example
* Transactions Control Language(TCL)
  - *Transaction*: Ability to rollback.
  - START TRANSACTION/BEGIN, COMMIT, ROLLBACK, SAVEPOINT and SET autocommit.
  - START TRANSACTION/BEGIN: Starts a new Transaction.
  - COMMIT: Complete the Transaction, After commit we can not revert
    the changes.
  - ROLLBACK: Must be executed before COMMIT, revert changes. After
    ROLLBACK no COMMIT.
  - SAVEPOINT: ROLLBACK the changes to save point.
  - SET autocommit: enables/disables default auto commit mode for the
    current session.
** COMMIT:
   - The COMMIT Statement complete the Transaction.
   - The Transaction can be INSERT, UPDATE and DELETE Queries.
   - After COMMIT no revert.
   - Example:
     #+begin_example sql
       START TRANSECTION; --or BEGIN;
       -- Insert/Update/Delete
       COMMIT;
     #+end_example
** ROLLBACK
   - The ROLLBACK revert back the changes.
   - It must be done before COMMIT Statement.
   - After ROLLBACK no COMMIT.
   - Example:
     #+begin_example sql
       START TRANSECTION; --or BEGIN;
       -- Insert/Update/Delete
       ROLLBACK;
     #+end_example
** SAVEPOINT
   - SAVEPOINT statement saves a transaction temporarily.
   - There can be multiple SAVEPOINT in single transaction.
   - ROLLBACK to selected SAVEPOINT.
   - No SAVEPOINT ROLLBACK after COMMIT.
   - Create a SAVEPOINT and ROLLBACK to a SAVEPOINT:
     #+begin_example sql
       -- Insert/Update/Delete;
       SAVEPOINT savepointname;
       ROLLBACK TO savepointname;
     #+end_example
* Database Relationship 
  - Relationship Between two table are created with keys. A key in one
    table will normally related to another table.
  - There are mainly Three type of relationship.
** One to One (1:1)
   - If only one data row in one table is related to only one data row
     in another table.
     [[./One_To_One.png]] 
** One to Many (1:M) (1:*) (1:N)
   - If only one data row in one table is related to many data row in
     another table.
     [[./One_To_Many.png]]
** Many to Many (M:M) (*:*) (N:N)
   - If many data row in one table is related to many data row in
     another table.
     [[./Many_To_Many.png]]
* Advance SQL
  - select random data
    #+BEGIN_SRC sql
      SELECT * FROM table_name ORDER BY rand(); 
    #+END_SRC
  - if else
    #+BEGIN_SRC sql
      -- if(condition, true value, flase value)
      SELECT name, IF(sex='boy', "M", "F") as sex FROM table_name;
    #+END_SRC
  - insert with if else
    #+BEGIN_SRC sql
      INSERT INTO table_one(col_one, col_two)
      SELECT name, IF(sex='boy', "M", "F") as sex FROM table_two; 
    #+END_SRC
  - Random number in SQL
    #+BEGIN_SRC sql
      -- random number between 45 to 65
      -- 65 - 45 = 20
      SELECT 45+CEIL(RAND()*20)
    #+END_SRC
  - Random value from a list with random function
    #+BEGIN_SRC sql
      -- ELT(number , val1, val2 ......)
      SELECT ELT( CEIL( RAND() * 5 ), 'A', 'B', 'C', 'D', 'E');
    #+END_SRC
  - Set random value in a table 
    #+BEGIN_SRC sql
      -- col_one 1-10
      -- col_two from list of 4
      UPDATE table_name SET
      col_one = CEIL(RAND() * 10),
      col_two = ELT(CEIL(RAND() * 4), 'A', 'B', 'C', 'D');
    #+END_SRC
  - use of variable and increment like loop
    #+BEGIN_SRC sql
      -- Setting up variable
      SET @foo=0
      UPDATE table_name SET
      col_one=@foo:=@foo+1 WHERE col_two=1 AND col_three='A'; 
    #+END_SRC
  - duplicate table with index and all
    #+BEGIN_SRC sql
      CREATE TABLE new_table LIKE old_table;
    #+END_SRC
