#+title: Basic of HTML
#+author: Shafiqur Rahman

* Basic Syntax
** We use ~DOCTYPE~ to load HTML5 correctly in browser.
   - Syntax 
     #+BEGIN_SRC html
       <!doctype html>
       <html lang="en">
	 <head>
	   <meta charset="UTF-8"/>
	   <title>Title Here</title>
	 </head>
	 <body>
	   <h1>Hello World!</h1>
	 </body>
       </html>
     #+END_SRC
* Tags
** Heading Tags
   - Examples
     #+BEGIN_SRC html
       <h1>Heading One</h1>
       <h2>Heading Two</h2>
       <h3>Heading Three</h3>
       <h4>Heading Four</h4>
       <h5>Heading Five</h5>
       <h6>Heading Six</h6>
     #+END_SRC
** Paragraph Tag
   - ~p~ use for paragraph tag. ~strong~ tag to bold text. ~em~ tag to
     make text italic.
     #+BEGIN_SRC html
       <p>
	 This is a Paragraph.
	 <strong>Bolding Words.</strong>
	 <em>Italic Style.</em>
       </p>
     #+END_SRC
** List Items
   - Ordered(Numbered) List
     #+BEGIN_SRC html
       <!-- Orderd (Number) List -->
       <ol>
	 <li>Item One</li>
	 <li>Item Two</li>
	 <li>Item Three</li>
	 <li>Item Four</li>
	 <li>Item Five</li>
       </ol>
     #+END_SRC
   - Unordered List
     #+BEGIN_SRC html
       <ul>
	 <li>Item 1</li>
	 <li>Item 2</li>
	 <li>Item 3</li>
	 <li>Item 4</li>
	 <li>Item 5</li>
       </ul>
     #+END_SRC
   - Nested List
     #+BEGIN_SRC html
       <!-- Nested List -->
       <ul>
	 <li>Item 1</li>
	 <li>Item 2</li>
	 <ol>
	   <li>Item 1</li>
	   <li>Item 2</li>
	   <li>Item 3</li>
	 </ol>
	 <li>Item 4</li>
	 <li>Item 5</li>
       </ul>
     #+END_SRC
** Self Closing Tags
*** Create a Horizontal Line and Give a line break
    - Horizontal Line
      #+BEGIN_SRC html
	<!-- Horzontal Line -->
	<hr>
      #+END_SRC
    - Line Break
      #+BEGIN_SRC html
	<!-- Line Break -->
	<br>
      #+END_SRC
*** Image Tag
     - Attributes of Image Tag
       - alt = Alt Text Shows When Image Not Load
       - src = Source Of The Image.
       - Examples
	 #+BEGIN_SRC html
	   <!-- When Image is in same folder  -->
	   <img alt="Default Text" src="logo.jpeg"/>      
	   <!-- When Image is in img folder  -->
	   <img alt="Default Text" src="img/logo.jpeg"/>
	 #+END_SRC
** Anchor Tag (hypertext reference tag)
   - Attribute of  ~a~ tag:  href = Here will be the lining source. It
     can be another file or another website.
     #+BEGIN_SRC html
       <!--Extarnal Link -->
       <a href="google.com">GO TO GOOGLE</a>
       <!--Internal Link -->
       <a href="about.html">GO ABOUT PAGE</a>
     #+END_SRC
** Form Tag
*** Attributes
    - action = Where to send the information process (To Server
      Mainly)
    - method = HTTP GET or POST method
      #+BEGIN_SRC html
	<form method="POST" action="login.php">
	</form>
      #+END_SRC
*** Different Types of Input Tags
    - Text Type for only Text
      #+BEGIN_SRC html
	<input name="Full Name" type="text" placeholder="Shafiqur Rahman"/>
      #+END_SRC
    - Password Type for only Password
      #+BEGIN_SRC html
	<input name="pwd" type="password" placeholder="Password"/>
      #+END_SRC
    - Email Type for only Email
      #+BEGIN_SRC html
	<input name="email" type="email" placeholder="example@email.com"/>
      #+END_SRC
    - Date Type for only Date 
      #+BEGIN_SRC html
	<input name="dob" type="date"/>
      #+END_SRC
    - Radio Type for only Radio Buttons
      #+BEGIN_SRC html
	<input name="gender" type="radio" /> Male
	<input name="gender" type="radio" /> Female
	<input name="gender" type="radio" /> Other
      #+END_SRC
    - Checkbox Type for only Checkbox Buttons
      #+BEGIN_SRC html
	<input name="cat" type="checkbox"/>Cat
	<input name="dog" type="checkbox"/>Dog
      #+END_SRC
    - Input Submit Type for Submit Button
      #+BEGIN_SRC html
	<input name="submit" type="submit" value="Submit"/>
      #+END_SRC
    - Input Reset Type for Reset Button
      #+BEGIN_SRC html
	<input name="reset" type="reset" value="Reset"/>
      #+END_SRC
*** Select Box
    - Make a select box 
      #+BEGIN_SRC html
	<select id="cars" name="cars">
	  <option value="audi">Audi</option>
	  <option value="volvo">Volvo</option>
	  <option value="suv">SUV</option>
	</select>
      #+END_SRC
** Div Tag and Span Tag
   - Div use for divide sections
     #+BEGIN_SRC html
       <div>Divide</div>
     #+END_SRC
   - Span use for special CSS adding
     #+BEGIN_SRC html
       <span>Special CSS</span>
     #+END_SRC
** Table Tag
   - border
   - ~rowspan~
   - ~colspan~
   - Example:
     #+BEGIN_SRC html
     
     #+END_SRC
** HTML5 Specific Tags
*** Header
    - HTML5 header section
      #+BEGIN_SRC html
	<header>
	  <h1>LOGO</h1>
	</header>
      #+END_SRC
*** ~Nav~
    - Navigation bar for the website
      #+BEGIN_SRC html
	<nav>
	  <ul>
	    <li><a href="">Home</a></li>
	    <li><a href="">About</a></li>
	    <li><a href="">Contact</a></li>
	  </ul>
	</nav>
      #+END_SRC
*** Main
    - 
*** Section
*** Article
*** Aside
*** Footer
